## Houseplant CTF 2020

* **CTF Time page:** https://ctftime.org/event/997 
* **Category:** jeopardy
* **Date:** Fri, 24 April 2020, 19:00 UTC — Sun, 26 April 2020, 19:00 UTC 

---

### Solved Challenges
| Name | Category | Points | Key Concepts |
|------|----------|--------|--------------|
|ez |rev |50 |python |
|pz |rev |50 |python |
|lemon |rev |50 |python |
|squeezy |rev |50 |python, XOR, base64 |
|thedanzman |rev |50 | python, XOR, base64, rot13 |
|bendy |rev |50 |java |
|breakable |rev |50 |java |
|satan's jigsaw |misc |704 |python, image |

### Unsolved Challenges to Revisit
| Name | Category | Points | Key Concepts |
|------|----------|--------|--------------|
|rivest-shamir-adleman |crypto |338 |RSA |
|returning stolen archives |crypto |50 |RSA |
|tough |rev |1237 |z3 |
