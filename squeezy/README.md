## Squeezy

* **CTF:** Houseplant CTF 2020
* **Category:** rev
* **Points:** 500
* **Author(s):** b0uldrr
* **Date:** 25/04/20

---

### Challenge
```
Ok this time, you aren't getting anywhere near anything.

Dev: William
```

### Downloads
* [pass3.py](pass3.py)

---

### Solution

I downloaded the provided source:

```python
import base64
def checkpass():
  userinput = input("Enter the password: ")
  key = "meownyameownyameownyameownyameownya"
  a = woah(key,userinput)
  b = str.encode(a)
  result = base64.b64encode(b, altchars=None)
  if result == b'HxEMBxUAURg6I0QILT4UVRolMQFRHzokRBcmAygNXhkqWBw=':
      return True
  else:
      return False

def main():
    access = checkpass()
    if access == True:
        print("Unlocked. The flag is the password.")
        print("pwease let me do my nya~ next time!!")
        exit()
    else:
        print("Incorrect password!")
        print("sowwy but now you gunnu have to listen to me spweak in cat giwrl speak uwu~")
        catmain()

def catmain():
    access = catcheckpass()
    if access == True:
        print("s-senpai... i unwocked it fowr you.. uwu~")
        print("t-the fwlag is... the password.. nya!")
        exit()
    else:
        print("sowwy but that wasnt quite rwight nya~... pwease twy again")
        catmain()

def catcheckpass():
  userinput = input("pwease enter youwr password... uwu~ nya!!: ")
  key = "meownyameownyameownyameownyameownya"
  a = woah(key,userinput)
  b = str.encode(a)
  result = base64.b64encode(b, altchars=None)
  if result == b'HxEMBxUAURg6I0QILT4UVRolMQFRHzokRBcmAygNXhkqWBw=':
      return True
  else:
      return False

def woah(s1,s2):
    return ''.join(chr(ord(a) ^ ord(b)) for a,b in zip(s1,s2))
access = False
main()
```

This program:
1. Takes a user input
1. XORs each character against each character of the string "meownyameownyameownyameownyameownya" 
1. Coverts the output to base64
1. Compares that output to the encoded flag value, "HxEMBxUAURg6I0QILT4UVRolMQFRHzokRBcmAygNXhkqWBw=" 

I wrote a script which takes the encrypted flag and reverses the process:

```python
#! /usr/bin/python3

import base64

# function taken from pass3.py source
def woah(s1,s2):
    return ''.join(chr(ord(a) ^ ord(b)) for a,b in zip(s1,s2))

enc = b'HxEMBxUAURg6I0QILT4UVRolMQFRHzokRBcmAygNXhkqWBw='
enc = base64.b64decode(enc)
enc = enc.decode()

key = "meownyameownyameownyameownyameownya"
flag = woah(key,enc)
print(flag)
```

---

### Flag 
```
rtcp{y0u_L3fT_y0uR_x0r_K3y_bEh1nD!}
```
