#! /usr/bin/python3

import base64

# function taken from pass3.py source
def woah(s1,s2):
    return ''.join(chr(ord(a) ^ ord(b)) for a,b in zip(s1,s2))

enc = b'HxEMBxUAURg6I0QILT4UVRolMQFRHzokRBcmAygNXhkqWBw='
enc = base64.b64decode(enc)
enc = enc.decode()

key = "meownyameownyameownyameownyameownya"
flag = woah(key,enc)
print(flag)
