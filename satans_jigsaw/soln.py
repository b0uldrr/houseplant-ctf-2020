#! /usr/bin/python3

from os import listdir
from PIL import Image
import numpy as np

# get a list of all of the image files in the unzipped directory
files = listdir("./chall")

# strip the ".jpg" from the end and convert the file names to integers so that we can sort them
for i in range(0, len(files)):
    files[i] = int(files[i][0:-4])

files.sort()

# create an empty numpty image array, 200 high, 450 wide, with a 3rd array for the RGB values
# This stackexchange post helped: https://stackoverflow.com/questions/10465747/how-to-create-a-white-image-in-python 
out = np.empty((200,450,3), dtype=np.uint8)

# open every file, extract the pixel value and store it in the output array
count = 0
for x in range(0, 450):
    for y in range(0, 200):
        img = Image.open("./chall/" + str(files[count]) + ".jpg")
        out[y,x] = img.getpixel((0,0))
        count += 1

# save our output array in the an image
flag = Image.fromarray(out)
flag.save('flag.jpg')
