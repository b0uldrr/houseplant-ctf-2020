## Satan's Jigsaw

* **CTF:** Houseplant CTF 2020
* **Category:** misc
* **Points:** 704
* **Author(s):** b0uldrr
* **Date:** 26/04/20

--- 
### Challenge
```
Oh no! I dropped my pixels on the floor and they're all muddled up! It's going to take me years to sort all 90,000 of these again :(

Dev: Tom 
```

### Hint
```
long_to_bytes
```

### Downloads
* [chall.7z](chall.7z)

---

### Solution

I extractred the provided 7z file. Inside were 90,000 jpg files, each just a single pixel. The goal was to put them back together into the original image. The 90,000 images all had numeric names which, although they clearly weren't random, didn't really play into my solution as intended by the challenge designer. My solution, shown below, sorted the pixels files into numeric order (as opposed to the alphabetical order that they are normally held in), extracts pixel RGB data and then stores it in a new numpy array which is finally transformed into an output image. The biggest challenge was trying to figure out the dimension that the original image was so that I could correctly reconstruct it. This was easy enough to change, by alterning the numpy array size and the x & y `for loops`. In the end, I found a size of 450 pixesl wide and 200 high displayed part of the picture correctly and showed a QR code, which gave the flag. See the image below.  

```python
#! /usr/bin/python3

from os import listdir
from PIL import Image
import numpy as np

# get a list of all of the image files in the unzipped directory
files = listdir("./chall")

# strip the ".jpg" from the end and convert the file names to integers so that we can sort them
for i in range(0, len(files)):
    files[i] = int(files[i][0:-4])

files.sort()

# create an empty numpty image array, 200 high, 450 wide, with a 3rd array for the RGB values
# This stackexchange post helped: https://stackoverflow.com/questions/10465747/how-to-create-a-white-image-in-python
out = np.empty((200,450,3), dtype=np.uint8)

# open every file, extract the pixel value and store it in the output array
count = 0
for x in range(0, 450):
    for y in range(0, 200):
        img = Image.open("./chall/" + str(files[count]) + ".jpg")
        out[y,x] = img.getpixel((0,0))
        count += 1

# save our output array in the an image
flag = Image.fromarray(out)
flag.save('flag.jpg')
```

![flag.jpg](flag.jpg)

However, in reading other writeups for this challenge, I found that the *correct* way to solve it was to follow the hint and convert the 90,000 file names to bytes, which gave the x & y coordinates for each pixel. This would have displayed the image in full without any errors.

---

### Flag 
```
rtcp{d1d-you_d0_7his_by_h4nd?}
```
