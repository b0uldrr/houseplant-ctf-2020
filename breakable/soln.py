#! /usr/bin/python3

enc  = "ÒdÝ¾¤¤¾ÙàåÐcÝÆ¥ÌÈáÏÜ¦aã" 
key  = "k33p_1t_in_pl41n"
flag = [None] * 16

count = 0
for c in enc[:14]:
    flag[count+2] = ord(c) - ord(key[count])
    count += 1

count = 2
for c in enc[14:]:
    flag[count-2] = ord(c) - ord(key[count])
    count += 1

print("rtcp{",end='')
for c in flag:
    print(chr(c),end='')
print("}")
