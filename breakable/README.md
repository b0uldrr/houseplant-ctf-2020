## breakable

* **CTF:** Houseplant CTF 2020
* **Category:** rev
* **Points:** 50
* **Author(s):** b0uldrr
* **Date:** 25/04/20

---

### Challenge
```
Okay...this one's better, but still be careful!

Dev: Sri 
```

### Downloads
* [breakable.java](breakable.java)

---

### Solution

We're provided with java source code which prompts for user input, does some stuff to that input, and then compares the output with the encrypted flag string. Our goal is to reverse the process to find the unencrypted flag string.

Here's the original source code. I added the "Loop" comments:

```java
import java.util.*;

public class breakable
{
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter flag: ");
        String userInput = scanner.next();
        String input = userInput.substring("rtcp{".length(),userInput.length()-1);
        if (check(input)) {
            System.out.println("Access granted.");
        } else {
            System.out.println("Access denied!");
        }
    }
    
    public static boolean check(String input){
        boolean h = false;
        String flag = "k33p_1t_in_pl41n";
        String theflag = "";
        int i = 0;
        if(input.length() != flag.length()){
            return false;
        }

	// "Loop 1"
        for(i = 0; i < flag.length()-2; i++){
            theflag += (char)((int)(flag.charAt(i)) + (int)(input.charAt(i+2)));
        }

	// "Loop 2"
        for(i = 2; i < flag.length(); i++){
            theflag += (char)((int)(flag.charAt(i)) + (int)(input.charAt(i-2)));
        }

	// "Loop 3"
        String[] flags = theflag.split("");
        for(; i < (int)((flags.length)/2); i++){
            flags[i] = Character.toString((char)((int)(flags[i].charAt(0)) + 20));
        }
        return theflag.equals("ÒdÝ¾¤¤¾ÙàåÐcÝÆ¥ÌÈáÏÜ¦aã");
    }
}
```
To help understand the program, I would regularly make small changes and print statements throughout the original source and then compile and run it. To compile:

```
$ javac breakable.java
```

This produces an executable java file, `breakable.class`, which can be run with: 
```
$ java breakable
```

Note that "Loop 3" in the original source does not ever run, because the value of `i` is greater than `(flags.length)/2` before the loop starts.

I created a python script to reverse the process and recover the flag:

```python
#! /usr/bin/python3

enc  = "ÒdÝ¾¤¤¾ÙàåÐcÝÆ¥ÌÈáÏÜ¦aã" 
key  = "k33p_1t_in_pl41n"
flag = [None] * 16

count = 0
for c in enc[:14]:
    flag[count+2] = ord(c) - ord(key[count])
    count += 1

count = 2
for c in enc[14:]:
    flag[count-2] = ord(c) - ord(key[count])
    count += 1

print("rtcp{",end='')
for c in flag:
    print(chr(c),end='')
print("}")
```

---

### Flag 
```
rtcp{0mg_1m_s0_pr0ud_}
```
