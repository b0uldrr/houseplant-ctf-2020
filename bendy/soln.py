#! /usr/bin/python3

theflag = "ÄÑÓ¿ÂÒêáøz§è§ñy÷¦"
flag    = "r34l_g4m3rs_eXclus1v3"
output  = [None] * 21 

# Turn theflag chars into a list to make it eaiser to work with
theflag = list(theflag)

# Convert all of theflag chars into their ordinal values now instead of calling ord() every time
for i in range(0,len(theflag)):
    theflag[i] = ord(theflag[i])

# "Loop 5"
theflag = theflag[9:] + theflag[:9]

# "Loop 4"
for i in range(0,9):
    theflag[i] = theflag[i] - 20

# "Loop 3"
for i in range(20, 14, -1):
    output[i] = theflag.pop() - ord(flag[i-3]) 

# "Loop 2"
for i in range(14, 9, -1):
    output[i-8] = theflag.pop() - ord(flag[i]) 

# "Loop 1"
for i in range(6, -1, -1):
    output[i+8] = theflag.pop() - ord(flag[i])

# print the output... note that some of the values are blank (replaced by "?" here), but we can guess what they are
print("rtcp{",end="")
for c in output:
    try:
        print(chr(c),end="")
    except:
        print("?",end="")
print("}")
