## bendy

* **CTF:** Houseplant CTF 2020
* **Category:** rev
* **Points:** 50 
* **Author(s):** b0uldrr
* **Date:** 26/04/20

---

### Challenge
```
I see you've found my straw collection...(this is the last excessive for loop one i swear)

Dev: Sri 
```

### Downloads
* [bendy.java](bendy.java)

---

### Solution

The provided java class prompts for a user input, does some stuff to the input text, then compares the outcome with the encrypted flag value. We need to reverse the process to find the unencrypted flag.

In trying to understand exactly how this java class works, I often added print statements throughout and then compiled and ran it.

To compile:
```
$ javac bendy.java
```

That produces a java executable file `bendy.class`, which you can run with:
```
$ java bendy
```

Here is the original source. I've added the "Loop" comments.

```java
import java.util.*;

public class bendy
{
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter flag: ");
        String userInput = scanner.next();
        String input = userInput.substring("rtcp{".length(),userInput.length()-1);
        if (check(input)) {
            System.out.println("Access granted.");
        } else {
            System.out.println("Access denied!");
        }
    }
    
    public static boolean check(String input){
        boolean h = false;
        String flag = "r34l_g4m3rs_eXclus1v3";
        String theflag = "";
        int i = 0;
        if(input.length() != flag.length()){
            return false;
        }

	// "Loop 1" 
        for(i = 0; i < flag.length()-14; i++){
            theflag += (char)((int)(flag.charAt(i)) + (int)(input.charAt(i+8)));
        }

	// "Loop 2" 
        for(i = 10; i < flag.length()-6; i++){
            theflag += (char)((int)(flag.charAt(i)) + (int)(input.charAt(i-8)));
        }

	// "Loop 3" 
        for(; i < flag.length(); i++){
            theflag += (char)((int)(flag.charAt(i-3)) + (int)(input.charAt(i)));
        }

	// "Loop 4" 
        String[] flags = theflag.split("");
        for(i=0; i < (int)((flags.length)/2); i++){
            flags[i] = Character.toString((char)((int)(flags[i].charAt(0)) + 20));
        }

	// "Loop 5"
        theflag = theflag.substring(flags.length/2);
        for(int k = 0; k < ((flags.length)/2); k++){
            theflag += flags[k];
        }

        return theflag.equals("ÄÑÓ¿ÂÒêáøz§è§ñy÷¦");
    }
}
```

I wrote a python script to reverse the process. Compare the "Loop" sections to see which particular part I was reversing.

```python
#! /usr/bin/python3

theflag = "ÄÑÓ¿ÂÒêáøz§è§ñy÷¦"
flag    = "r34l_g4m3rs_eXclus1v3"
output  = [None] * 21 

# Turn theflag chars into a list to make it eaiser to work with
theflag = list(theflag)

# Convert all of theflag chars into their ordinal values now instead of calling ord() every time
for i in range(0,len(theflag)):
    theflag[i] = ord(theflag[i])

# "Loop 5"
theflag = theflag[9:] + theflag[:9]

# "Loop 4"
for i in range(0,9):
    theflag[i] = theflag[i] - 20

# "Loop 3"
for i in range(20, 14, -1):
    output[i] = theflag.pop() - ord(flag[i-3]) 

# "Loop 2"
for i in range(14, 9, -1):
    output[i-8] = theflag.pop() - ord(flag[i]) 

# "Loop 1"
for i in range(6, -1, -1):
    output[i+8] = theflag.pop() - ord(flag[i])

# print the output... note that some of the values are blank (replaced by "?" here), but we can guess what they are
print("rtcp{",end="")
for c in output:
    try:
        print(chr(c),end="")
    except:
        print("?",end="")
print("}")
```
Running the python script will restore all of the original flag exept for 3 characters (see the '?' characters in the output below). We can guess what they are meant to be by the context.

```
$ ./soln.py 
rtcp{??p3_y0?r3_h4v1ng_fun}
```
---

### Flag 
```
rtcp{h0p3_y0ur3_h4v1ng_fun}
```
