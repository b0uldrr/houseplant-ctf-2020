## thedanzman

* **CTF:** Houseplant CTF 2020
* **Category:** rev
* **Points:** 70
* **Author(s):** b0uldrr
* **Date:** 25/04/20

---

### Challenge
```
Fine. I made it even harder. It is now no longer "ez", "pz", "lemon" or "squeezy".
You will never get the flag this time.

Dev: William 
```

### Hint
```
This should be no problem if you look at the previous ones
```

### Downloads
* [pass4.py](pass4.py)

---

### Solution

I downloaded the provided source code:

```python
import base64
import codecs
def checkpass():
  userinput = input("Enter the password: ")
  key = "nyameowpurrpurrnyanyapurrpurrnyanya"
  key = codecs.encode(key, "rot_13")
  a = nope(key,userinput)
  b = str.encode(a)
  c = base64.b64encode(b, altchars=None)
  c = str(c)
  d = codecs.encode(c, 'rot_13')
  result = wow(d)
  if result == "'=ZkXipjPiLIXRpIYTpQHpjSQkxIIFbQCK1FR3DuJZxtPAtkR'o":
      return True
  else:
      return False

def main():
    access = checkpass()
    if access == True:
        print("Unlocked. The flag is the password.")
        print("pwease let me do my nya~ next time!!")
        exit()
    else:
        print("Incorrect password!")
        print("sowwy but now you gunnu have to listen to me spweak in cat giwrl speak uwu~")
        catmain()

def catmain():
    access = catcheckpass()
    if access == True:
        print("s-senpai... i unwocked it fowr you.. uwu~")
        print("t-the fwlag is... the password.. nya!")
        exit()
    else:
        print("sowwy but that wasnt quite rwight nya~... pwease twy again")
        catmain()

def catcheckpass():
  userinput = input("pwease enter youwr password... uwu~ nya!!: ")
  key = "nyameowpurrpurrnyanyapurrpurrnyanya"
  key = codecs.encode(key, "rot_13")
  a = nope(key,userinput)
  b = str.encode(a)
  c = base64.b64encode(b, altchars=None)
  c = str(c)
  d = codecs.encode(c, 'rot_13')
  result = wow(d)
  if result == "'=ZkXipjPiLIXRpIYTpQHpjSQkxIIFbQCK1FR3DuJZxtPAtkR'o":
      return True
  else:
      return False

def nope(s1,s2):
    return ''.join(chr(ord(a) ^ ord(b)) for a,b in zip(s1,s2))

def wow(x):
  return x[::-1]

access = False
main()
```

This program:
1. Takes a user input
1. XORs each character against the string "nyameowpurrpurrnyanyapurrpurrnyanya"
1. Encodes it in base64
1. Conducts a rot13
1. Compares the output to the encrypted flag value, "'=ZkXipjPiLIXRpIYTpQHpjSQkxIIFbQCK1FR3DuJZxtPAtkR'o"

I wrote a script that took the encrypted flag value and reversed the process.

key = "nyameowpurrpurrnyanyapurrpurrnyanya"
  key = codecs.encode(key, "rot_13")
  a = nope(key,userinput)
  b = str.encode(a)
  c = base64.b64encode(b, altchars=None)
  c = str(c)
  d = codecs.encode(c, 'rot_13')
  result = wow(d)
  if result == "'=ZkXipjPiLIXRpIYTpQHpjSQkxIIFbQCK1FR3DuJZxtPAtkR'o":
   
```python
#!/usr/bin/python3
import base64
import codecs

def nope(s1,s2):
    return ''.join(chr(ord(a) ^ ord(b)) for a,b in zip(s1,s2))

def wow(x):
  return x[::-1]

enc = "'=ZkXipjPiLIXRpIYTpQHpjSQkxIIFbQCK1FR3DuJZxtPAtkR'o"
key = "nyameowpurrpurrnyanyapurrpurrnyanya"

# un-reverse
enc = wow(enc)

# rot it by 13
enc = codecs.encode(enc, 'rot_13')

# strip the leading b' and trailing '
enc = enc[2:-1]

# encode the string as bytes
enc = str.encode(enc)

# decode base64
enc = base64.b64decode(enc)

# decode bytes to string
enc = enc.decode()

# rot the key by 13
key = codecs.encode(key, "rot_13")

# xor key with our string
flag = nope(key, enc)

print(flag)
```

---

### Flag 
```
rtcp{n0w_tH4T_w45_m0r3_cH4lL3NgiNG}
```
