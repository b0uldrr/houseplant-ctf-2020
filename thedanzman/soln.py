#!/usr/bin/python3
import base64
import codecs

def nope(s1,s2):
    return ''.join(chr(ord(a) ^ ord(b)) for a,b in zip(s1,s2))

def wow(x):
  return x[::-1]

enc = "'=ZkXipjPiLIXRpIYTpQHpjSQkxIIFbQCK1FR3DuJZxtPAtkR'o"
key = "nyameowpurrpurrnyanyapurrpurrnyanya"

# un-reverse
enc = wow(enc)

# rot it by 13
enc = codecs.encode(enc, 'rot_13')

# strip the leading b' and trailing '
enc = enc[2:-1]

# encode the string as bytes
enc = str.encode(enc)

# decode base64
enc = base64.b64decode(enc)

# decode bytes to string
enc = enc.decode()

# rot the key by 13
key = codecs.encode(key, "rot_13")

# xor key with our string
flag = nope(key, enc)

print(flag)
